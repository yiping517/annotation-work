package test;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 定義一個可以註解在class,interface,enum上的註解
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface MyAnTargetType {
	/**
	 * 定義註解的一個元素，並給定預設值
	 */
	String value() default "我是定義在類介面列舉類上的註解元素value的預設值";
}
