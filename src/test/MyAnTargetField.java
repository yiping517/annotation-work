package test;
/**
 * 定義一個可以註解在field上的註解
 */

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface MyAnTargetField {
	/**
	 * 定義註解的一個元素，並給定預設值
	 */
	String value() default "我是定義在欄位上的註解元素value的預設值";
}
