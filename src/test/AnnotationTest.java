package test;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
@MyAnTargetType
public class AnnotationTest {

	@MyAnTargetField
	private String field = "欄位";
	
	@MyAnTargetMethod("測試方法")
	public void test(@MyAnTargetParameter String args) {
		System.out.println("引數值 = "+args);
	}
	public static void main(String[] args) {
		//獲取類上的註解
		MyAnTargetType t = AnnotationTest.class.getAnnotation(MyAnTargetType.class);
		System.out.println("類上的註解值 = "+t.value());
		MyAnTargetMethod tm = null;
		try {
			//根據反射，獲取類上的註解MyAnTargetType
			Method method = AnnotationTest.class.getDeclaredMethod("test",String.class);
			tm = method.getAnnotation(MyAnTargetMethod.class);
			System.out.println(tm.value());
			//獲取方法上的所有引數註解、迴圈所有註解找到MyAnTargetParameter註解
			Annotation[][] annotations = method.getParameterAnnotations();
			for(Annotation[] tt:annotations) {
				for(Annotation t1:tt) {
					if(t1 instanceof MyAnTargetParameter) {
						System.out.println("引數上的註解值 = "+((MyAnTargetParameter)t1).value());
					}
				}
			}
			method.invoke(new AnnotationTest(), "改變預設引數");
			//獲取AnnotationTest類上欄位field的註解MyAnTargetField
			MyAnTargetField fieldAn = AnnotationTest.class.getDeclaredField("field").getAnnotation(MyAnTargetField.class);
			System.out.print("欄位上的註解值 : "+fieldAn.value());
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchFieldException e) {
			e.printStackTrace();
		}
	}

}
